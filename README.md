# TreehouseSimpleDynamicSiteWithNode
My code for the Treehouse course "Build a Simple Dynamic Site with node.js".

Note: I was unable to get a preview of this code to show in my browser, even after making sure to kill and restart all the running node processes. I tried on multiple ports, multiple machines and in multiple browsers, and I double checked with the video that my code mirrored his exactly. I'm not sure what the problem is. I even went back to my workspace for my last Treehouse course, and I got the same error.

Since this was an issue from the beginning of the course, I don't think it has to do with my code. 

Second issue: When I run the code in the workspace console, I get an uncaught error exception, but it's from the events.js file, which I don't have access to. Also not sure why this is occuring.
